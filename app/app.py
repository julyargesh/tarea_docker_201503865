from typing import List, Dict
from flask import Flask
import mysql.connector
import json

app = Flask(__name__)


def clientes() -> List[Dict]:
    config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'SISTEMA'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM CLIENTE')
    results = [{NOMBRE: EDAD} for (NOMBRE, EDAD) in cursor]
    cursor.close()
    connection.close()

    return results


@app.route('/')
def index() -> str:
    return json.dumps({'CLIENTES': clientes()})


if __name__ == '__main__':
    app.run(host='0.0.0.0')